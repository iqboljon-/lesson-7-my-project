let elStartBtn = document.querySelector('.start-btn');
let elStopBtn = document.querySelector('.stop-btn');
let elResetBtn = document.querySelector('.reset-btn');

let setMinutes = document.querySelector('.minutes');
let setSeconds = document.querySelector('.seconds');


if (window.localStorage.getItem("time") === null) {
    window.localStorage.setItem(
      "time",
      JSON.stringify({ minutes: "00", seconds: "00" })
    );
    console.log(true);
}
  let minutes = JSON.parse(window.localStorage.getItem("time")).minutes;
  let seconds = JSON.parse(window.localStorage.getItem("time")).seconds;
  
  setMinutes.innerHTML = minutes;
  setSeconds.innerHTML = seconds;
  let Interval;

  
  elStartBtn.addEventListener("click", () => {
    clearInterval(Interval);
    Interval = setInterval(startTime, 10);
  });
  
  elStopBtn.addEventListener("click", () => {
    clearInterval(Interval);
    window.localStorage.setItem("time", JSON.stringify({ minutes, seconds }));
  });
  
  elResetBtn.addEventListener("click", () => {
    clearInterval(Interval);
    window.localStorage.removeItem("time");
  
    seconds = "00";
    minutes = "00";
    setMinutes.innerHTML = minutes;
    setSeconds.innerHTML = seconds;
  });
  
  function startTime() {
    seconds++;
    if (seconds <= 60) {
      setSeconds.innerHTML = "0" + seconds;
    }
  
    if (seconds > 9) {
      setSeconds.innerHTML = seconds;
    }
  
    if (seconds > 60) {
      minutes++;
      setMinutes.innerHTML = "0" + minutes;
      seconds = 0;
      setSeconds.innerHTML = "0" + seconds;
    }
  
    if (minutes > 9) {
      setMinutes.innerHTML = minutes;
    }
    window.localStorage.setItem("time", JSON.stringify({ minutes, seconds }));
  }